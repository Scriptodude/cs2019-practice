module DirSize (allFilesParent, allFilesSub) where

import System.Directory (listDirectory, makeAbsolute)
import Data.Text (pack, count) 

appendParent x = x ++ "/../"

allFilesParent :: Int -> FilePath -> IO [[FilePath]]
allFilesParent maxParentVisit path  = sequence $ listFiles path 0 
    where 
        listFiles :: FilePath -> Int -> [IO [FilePath]]
        listFiles path parentCount
            | parentCount >= maxParentVisit = [listDirectory path]
            | otherwise = 
                [listDirectory path] 
                ++ listFiles (appendParent path) (parentCount + 1)

allFilesSub :: Int -> FilePath -> IO [[FilePath]]
allFilesSub maxDepth path = sequence $ listFiles maxDepth path
    where 
        listFiles :: Int -> FilePath -> [IO [FilePath]]
        listFiles depthRem path
            | depthRem == 0 = [listDirectory path]
            | otherwise = 
                [listDirectory path] 
                ++ [listDirectory <$> (listDirectory path)]