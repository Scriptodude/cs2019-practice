import DirSize
import Data.Char
import System.Directory (getCurrentDirectory)

guardOverflow :: Int -> Int
guardOverflow x 
    | x < 0 = 0
    | (abs x) >= 32 = 32
    | otherwise = x

validateIntString :: String -> Int
validateIntString inp   
    | (and $ map isDigit inp) == True = 
        let value = (read inp) :: Int in 
            guardOverflow value
    | otherwise = 1

main = do
    putStrLn "Please enter a number."
    parentVisit <- validateIntString <$> getLine
    files <- getCurrentDirectory >>= allFilesParent parentVisit
    files2 <- getCurrentDirectory >>= allFilesSub parentVisit
    mapM_ (putStrLn . show) files
    mapM_ (putStrLn . show) files2
    putStrLn "Done"
