module CoveoSpec (spec) where

import Test.Hspec
import CoveoWater

spec :: Spec
spec = do
    describe "<= 5 elements" $ do
        it "works with 2 1 2" $ do 
            solve [2,1,2] `shouldBe` 1
        it "works with 3 1 2" $ do
            solve [3,1,2] `shouldBe` 1
        it "works with 2 1 3" $ do 
            solve [2,1,3] `shouldBe` 1
        it "works with 1 2 1 3" $ do
            solve [1,2,1,3] `shouldBe` 1
        it "works with 10 3 1 9" $ do
            solve [10,3,1,9] `shouldBe` 14
        it "works with 10 1 8 1 6" $ do
            solve [10,1,8,1,6] `shouldBe` 12
        it "works with 1 2 1 3 2" $ do
            solve [1,2,1,3,2] `shouldBe` 1
        it "works with 1 1 1 1 1" $ do
            solve [1,1,1,1,1] `shouldBe` 0
        it "works with 1 2 3 2 1" $ do
            solve [1,2,3,2,1] `shouldBe` 0
        it "works with 3 2 1 2 3" $ do
            solve [3,2,1,2,3] `shouldBe` 4
        it "works with 5 3 1 2 2" $ do
            solve [5,3,1,2,2] `shouldBe` 1
        it "works with 4 3 4 1 2" $ do
            solve [4,3,4,1,2] `shouldBe` 2
        it "works with 2 1 2 1 2" $ do
            solve [2,1,2,1,2] `shouldBe` 2 
            
    describe "complex problems > elements" $ do
        it "works with 2 1 2 1 2 3 4 3 4 1 2" $ do
            solve [2,1,2,1,2,3,4,3,4,1,2] `shouldBe` 4
        it "works with 5,3,7,2,6,4,5,9,1,2" $ do
            solve [5,3,7,2,6,4,5,9,1,2] `shouldBe` 14