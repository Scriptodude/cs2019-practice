module LibSpec (spec) where

import Test.Hspec
import CoveoWater
import CoveoWaterData

spec :: Spec
spec = do
    describe "pillar tests" $ do
        it "returns true when neighbors are smaller" $ do
            isAPillar (1, 2, 1) `shouldBe` True
        it "returns true when neighbors are equal" $ do
            isAPillar (2, 2, 2) `shouldBe` True
        it "returns false when both neighbors are greater" $ do
            isAPillar (3, 2, 3) `shouldBe` False
        describe "returns false when one neighbor is greater" $ do
            it "Take 1" $ do
                isAPillar (3, 2, 2) `shouldBe` False
            it "Take 2" $ do
                isAPillar (3, 2, 1) `shouldBe` False
            it "Take 3" $ do
                isAPillar (1, 2, 3) `shouldBe` False
            it "Take 4" $ do
                isAPillar (2, 2, 3) `shouldBe` False

    describe "my iterate tests" $ do
        it "Works with the example array" $ do
            myIterate [1,2,1,2,3] `shouldBe` [Height 1, Pillar 2, Height 0, Height 1, Height 2, Pillar 3]
        it "Works with several examples" $ do
            myIterate [3,1,1,3] `shouldBe` [Pillar 3, Height 0, Height 1, Height 1, Pillar 3]
            myIterate [3,1,3] `shouldBe` [Pillar 3, Height 0, Height 1, Pillar 3]
            myIterate [1,2,3,2,1] `shouldBe` [Height 1, Height 2, Pillar 3, Height 0, Height 2, Height 1]
        it "Works with corner cases" $ do
            myIterate [1,2] `shouldBe` [Height 1, Pillar 2]
            myIterate [1] `shouldBe` []
            myIterate [1,1,1,1,1] `shouldBe` [Pillar 1, Height 0, Pillar 1, Height 0, Pillar 1, Height 0, Pillar 1, Height 0, Pillar 1]
