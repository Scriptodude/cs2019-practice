module CoveoWaterData where

data Block a = Height a | Pillar a deriving (Show, Eq)
instance (Ord a) => Ord (Block a) where
    Pillar a `compare` Pillar b = a `compare` b
    Pillar a `compare` Height b = a `compare` b
    Height a `compare` Height b = a `compare` b

toValue :: Block a -> a
toValue (Pillar a) = a
toValue (Height a) = a