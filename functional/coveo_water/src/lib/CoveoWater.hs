module CoveoWater where

import CoveoWaterData
import Debug.Trace

debug = flip trace

isAPillar :: (Integer, Integer, Integer) -> Bool
isAPillar (previous, current, next) = current >= previous && current >= next

{- Iterates over a list and generate a list of blocks using trios of numbers.
for instance the list [1,2,1,2,3] would generate [Height 1, Pillar 2, Height 1, Height 2, Pillar]

-}
myIterate :: [Integer] -> [Block Integer]
myIterate (_:[]) = []
myIterate [] = []
myIterate arr = myIterate_ 0 arr 
    where 
        myIterate_ :: Integer -> [Integer] -> [Block Integer]

        -- We need to have a initial pattern to take only 2 elements initially
        -- Then we take 3 to check each neighbors
        myIterate_ 0 (x:x2:xs) = 
            if isAPillar (0, x, x2) 
                then Pillar x: Height 0: myIterate_ 1 (x:x2:xs)
            else Height x: myIterate_ 1 (x:x2:xs)

        myIterate_ 0 _ = undefined -- Should never happen !

        -- Special case for the last 2 elements
        -- We stop the recursion
        myIterate_ _ (x:x2:[]) = 
            if isAPillar (x, x2, 0) 
                then [Pillar x2]
            else [Height x2]

        -- regular case
        myIterate_ _ (x:x2:x3:xs) =
            if isAPillar (x, x2, x3) 
                then Pillar x2: Height 0: myIterate_ 1 (x2:x3:xs)
            else Height x2: myIterate_ 1 (x2:x3:xs)
        myIterate_ _ [] = []
        myIterate_ _ [_] = undefined -- should never happen !

splitByPillars :: [Block Integer] -> [[Block Integer]]
splitByPillars (Pillar x:xs) = 
    filter has2Pillars $ ((Pillar x):takeWhile shouldTake (drop 1 xs)):splitByPillars xs
    where 
        shouldTake (Pillar _) = True
        shouldTake (Height 0) = False 
        shouldTake (Height _) = True
        has2Pillars array = 
            let notPillar (Height _) = False
                notPillar (Pillar _) = True
                in (length $ filter notPillar array) == 2

splitByPillars (Height _:xs) = splitByPillars xs
splitByPillars [] = []

printShit acc x y = "Acc : " ++ (show acc) ++ ", x = " ++ (show x) ++ ", y = " ++ (show y)

solve :: [Integer] -> Integer 
solve arr = sum $ map calculate $ (splitByPillars . myIterate) arr `debug` ("solving " ++ show arr)
    where 
        calculate :: [Block Integer] -> Integer
        calculate (_:[]) = 0
        calculate (_:_:[]) = 0
        calculate [] = 0
        calculate values = 
            let h = toValue $ head values
                t = toValue $ last values
                ma = max h t
                mi = min h t
                body = map toValue $ (init . tail) values
            in sum (map (mi-) $ filter (<mi) body) `debug` ("Values = " ++ show values)