module Main where

{-
That moment when you google to see other's solution
and find a 5 liners... RIP I need more practice !

rainfall :: [Int] -> Int
rainfall xs = sum (zipWith (-) mins xs)
    where mins = zipWith min maxl maxr
          maxl = scanl1 max xs
          maxr = scanr1 max xs

It basically does the same thing than I do,
but a lot more efficiently and readbly.

They find each "maximum pillar" on the left
then each "maximum pillar" on the right, 
apply the minimum in-between, substract that each values
from that minimum and sum it up. Here's a main that shows the
step. The last step is simply the sum.
-}

main :: IO ()
main = do
    putStrLn $ show $ maxl 
    putStrLn $ show $ maxr
    putStrLn $ show $ mins
    putStrLn $ show $ zipped
    where 
        maxl = scanl1 max [2,1,2,1,2,3,4,3,4,1,2]
        maxr = scanr1 max [2,1,2,1,2,3,4,3,4,1,2]
        mins = zipWith min maxl maxr
        zipped = zipWith (-) mins [2,1,2,1,2,3,4,3,4,1,2]
