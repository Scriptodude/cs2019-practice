import Control.Monad.State
import Control.Monad.Identity  
  
type Stack = [Int]

pop :: StateT Stack Identity Int  
pop = StateT $ \(x:xs) -> Identity (x,xs)  
  
push :: Int -> StateT Stack Identity ()  
push a = StateT $ \xs -> Identity ((),a:xs)  