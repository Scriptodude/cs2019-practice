module HtmlRequest where

import HttpTypes

getRequestLine :: String -> Maybe Request
getRequestLine line = return Request (lineSplit !! 0) (lineSplit !! 1) 
    where lineSplit = words line