# Revision history for data-fun

## 0.1.1.0 -- 2019-01-29

* Implemented HtmlTypes
* Implemented the initial version of QueryString module to parse query string

## 0.1.0.0 -- 2019-01-29

* First version. Released on an unsuspecting world.