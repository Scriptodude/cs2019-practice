module Main where

import System.IO
import FileParser
import qualified Data.List.GroupBy as GB
import Data.List

soilTypeEq x y = let (_,t1) = x; (_,t2) = y in t1 == t2
soilTypeSort x y = let (_,t1) = x; (_,t2) = y in t1 `compare` t2
positionEq x y = let (p1, _) = x; (p2, _) = y in p1 == p2
positionSort x y = let (p1, _) = x; (p2, _) = y in p1 `compare` p2
getCarbon (x:y:[]) = let (_, t) = x; (_, c) = y in (c, t)

main :: IO ()
main = do 
    soils <- openFile "resources/soil.txt" ReadMode >>= parseInput
    carbon <- openFile "resources/carbon.txt" ReadMode >>= parseInput
    putStrLn $ show soils
    putStrLn $ show carbon
    putStrLn $ show (soils ++ carbon)
    let grp = GB.groupBy positionEq $ sortBy positionSort (soils ++ carbon) in do
        putStrLn $ show $ map getCarbon grp
        let grp2 = GB.groupBy soilTypeEq $ sortBy soilTypeSort $ map getCarbon grp in do
            putStrLn $ show $ map (foldl (\acc x -> let (a, _) = x in acc + a) 0) grp2
