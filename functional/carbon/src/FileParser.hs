module FileParser (Position, Format, parseInput) where

import System.IO

type Position = (Int, Int)
type Format = (Position, Int)

createFormat (x:y:z:[]) = ((x, y), z)

parseInput :: Handle -> IO [Format]
parseInput file = do
    content <- hGetContents file
    return $ map (createFormat . (map read) . words) $ lines content