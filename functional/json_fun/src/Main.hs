module Main where

import Text.JSON

writeToFile = writeFile "test.json"

main :: IO ()
main = do
    let indexes = (++) <$> ["Chat", "Chien", "Oiseau"] <*> ["Préfère", "Souhaite", "Veut"]
        values = cycle ["Dormir", "Boire", "Mourir"]
        ziped = zip indexes values 
        in
            writeToFile . show . encodeStrict . toJSObject $ ziped
