npm install
npm test

if [ $? -eq 0 ]; then
    rm -rf client/release/static/javascript/*.js
    tsc --outDir client/release/static/javascript/ client/src/*.ts

    cd server/src/
    supervisor main.js
fi