const getUsers = async () => {
    const resp = await fetch("/user");
    const data = await resp.json();

    return data;
}

const users = getUsers().then((content) => {
    const ul = document.getElementById("test");
    for(let user of content) {
        let li = document.createElement("li");
        li.innerHTML = user.firstName + " " + user.middleName + " " + user.lastName;
        ul.appendChild(li);
    }
});