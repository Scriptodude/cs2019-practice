-- Create the table in the specified schema
CREATE TABLE User
(
    userId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, -- primary key column
    firstName VARCHAR(50) NOT NULL,
    middleName VARCHAR(50) NOT NULL,
    lastName VARCHAR(50) NOT NULL
);
GO