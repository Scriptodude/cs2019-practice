const sqlite = require("sqlite3").verbose()
const db = new sqlite.Database(':memory:')
const filesys = require("fs")

filesys.readFile("migration.sql", "utf8", (err, contents) => {
    if(!err) {
        db.serialize(() => {
            db.run(contents)

            console.log("Created db")
        })
    } else {
        console.log("Couldn't  create db !")
    }
})

module.exports.addUser = (user, callback) => {
    console.log(JSON.stringify(user))

    const insert = db.run(
        `INSERT INTO
            User(firstName, middleName, lastName) 
        VALUES (?,?,?)`, 
        [user.firstname, user.middlename, user.lastname], callback)
}

module.exports.getAllUsers = (callback) => {
    db.all("SELECT firstName, middleName, lastName FROM User;", [], callback)
}

module.exports.done = () => {
    db.close();
}