const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const userDAO = require("./userDAO");
app.listen(8080);
app.use(bodyParser.raw());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('../../client/release/'))

app.get("/user", function(req, resp) {
    const callback = (err, rows) => {
        resp.send(JSON.stringify(rows))
    }

    userDAO.getAllUsers(callback)
})

app.post("/add/user", function(req, resp) {
    const data = req.body

    console.log(JSON.stringify(data))
    const newUser = {}
    newUser.firstname = data.firstName
    newUser.middlename = data.middleName
    newUser.lastname = data.lastName

    const callback = (err) => {
        if (err) {
            console.log(err)
            resp.statusCode = 500
        } else {
            console.log("Successfully added " + JSON.stringify(newUser))
            resp.statusCode = 201
        }

        resp.redirect("/");
    }

    userDAO.addUser(newUser, callback)
})

function exitHandler(options, exitCode) {
    if (options.cleanup) console.log('clean');
    if (exitCode || exitCode === 0) console.log(exitCode);
    if (options.exit) process.exit();

    userDAO.done();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));